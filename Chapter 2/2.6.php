<?php
//6.  В  массиве  А(N)  максимальные  элементы,  являющиеся  четными  числами, заменить значениями их индексов.  
require "ArrayChapter.php";
class Ex2_6 extends ArrayChapter
{
	function getOddMax()
	{
		$maxNum = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] >= $maxNum && $this->arr[$i] % 2 == 0)
			{
				$maxNum = $this->arr[$i];
				$key = $i;
			}
		}
		return $key;
	}
	function swapKeys()
	{
		$key = self::getOddMax();
		$this->arr[$key] = $key;
		return true;
	}
	function execute()
	{
		if(self::swapKeys())
		{
			echo "New array - \n";
			print_r($this->arr);
		}
		else
		{
			echo "error";
		}
		
	}
}
$array = new Ex2_6;
$array->print();
$array->execute();
?>