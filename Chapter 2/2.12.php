<?php
//12. В массиве А(N) найти наибольший из всех отрицательных элементов и наименьший из всех положительных.php
require "ArrayChapter.php";
class Ex2_12 extends ArrayChapter
{
	function getMaxNegative()
	{
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] < 0)
			{
				if(!isset($maxNum))
				{
					$maxNum = $this->arr[$i];
				}
				if($this->arr[$i] >= $maxNum)
				{
					$maxNum = $this->arr[$i];
				}
			}
		}
		return $maxNum;
	}
	function getMinPositive()
	{
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] > 0)
			{
				if(!isset($minNum))
				{
					$minNum = $this->arr[$i];
				}
				if($this->arr[$i] <= $minNum)
				{
					$minNum = $this->arr[$i];
				}
			}
		}
		return $minNum;
	}
	function execute()
	{
		echo "\nнаибольший из всех отрицательных - " . self::getMaxNegative();
		echo "\nнаименьший из всех положительных - " . self::getMinPositive();
	}
}

$array = new Ex2_12;
$array->print();
$array->execute();
?>