<?php
// 31. Вычислить значение многочлена и всех его производных в заданной точке x (коэффициенты хранятся в массивах ).  
$arr = [5, 15, 20, -2, 15]; // 5+15x+20x^2-2x^3+15x^4
$x = readline("x- ");

//значение многочлена
echo getPolynomValue($arr, $x) . "\n";

$maxDerivative = count($arr); 
for($i = 0; $i < $maxDerivative-1; ++$i)
{
	//значение производной многочлена
	$arr = getDerivative($arr);
	echo getPolynomValue($arr, $x) . "\n";
}

function getPolynomValue($arr, $x)
{
	$value = 0;
	$str = "";
	for($i = 0; $i < count($arr); ++$i)
	{
		$arr[$i] >= 0 ? $sign = "+" : $sign = "";
		$str .= $sign.$arr[$i]."x^".$i;
		$value += $arr[$i]*($x**$i);
	}
	return $str . "=" . $value;
}

function getDerivative($arr)
{
	$newArr = array();
	for($i = 1; $i < count($arr); ++$i)
	{
		$newArr[] = $arr[$i]*$i;
	}
	return $newArr;
}

?>

