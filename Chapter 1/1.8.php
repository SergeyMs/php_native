<?php
//8. Получить все четырехзначные целые числа, в записи которых нет одинаковых цифр.

for ($i=1000;$i<=9999;$i++) {
    $value = $i;
    
    while ($value >= 1) {
        $num = $value % 10;
        $value = $value / 10;
        $cond = checkMatches($num, $value);
        if (!$cond) break;
    }
    if($cond) echo $i." ";

}

function checkMatches($numPrev, $value) {
    while ($value >=1) {
        $num = $value % 10;
        if ($numPrev == $num) {
            return false;
        }
        $value = $value / 10;
    }
    return true;
}
