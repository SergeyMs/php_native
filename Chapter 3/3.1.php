<?php
//1. В  массиве  А(N,N)  найти  суммы  элементов,  расположенных  на  главной диагонали, ниже диагонали и выше диагонали.  

$arr = array(
	[-4, -1,   9,  1,   7],
	[ 4,  0,  -6, -2, -10],
	[ 1,  7,  -4, -5,   2],
	[-2,  9, -10,  3,  10],
	[-6, -6,  -5,  5,   4]
);

$aboveDiagElementSum = 0;
$belowDiagElementSum = 0;
$onDiagElementSum = 0;

for($i = 0; $i < count($arr); ++$i)
{
	for($j = 0; $j < count($arr[0]); ++$j)
	{
		if ($i < $j)  $aboveDiagElementSum += $arr[$i][$j];
		if ($i == $j) $onDiagElementSum += $arr[$i][$j];
		if ($i > $j)  $belowDiagElementSum += $arr[$i][$j];
	}
}
echo "above diag - $aboveDiagElementSum\n" .
	"onDiagElementSum - $onDiagElementSum\n" .
	"belowDiagElementSum - $belowDiagElementSum\n";
?>
