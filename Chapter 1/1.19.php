<?
//19. Найти наименьшее общее кратное  (НОК)  двух натуральных чисел N и M.  

$N = readline("Enter N - ");
$M = readline("Enter M - ");


print(nok($N,$M));


function nok($n, $m) {
	
	if ($m < $n) {
		$temp = $n;
		$n = $m;
		$m = $temp;
	}

	if ($n == 1) return $m;

	$nok = $n+$m;

	while ($nok % $m != 0 && $nok % $n != 0) {
		$nok += $n;
	}
	
	return $nok-$m;
}