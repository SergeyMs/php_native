<?php
//35. В  массиве  А(N)  каждый  элемент  равен  0,  1  или  2.  Переставить   элементы массива так, чтобы сначала расположились все нули, затем все  двойки и, наконец, все единицы.   
$arr = [2, 1, 0, 2, 0, 1, 0, 2, 0, 0, 1, 1, 1, 2, 2, 2];
arraySort($arr);
print_r($arr);

function arraySort(&$arr)
{
	for($i = 0; $i < count($arr); ++$i)
	{
		$maxNumber = $arr[$i];
		for($j = $i; $j < count($arr); ++$j)
		{
			if ( 
				($arr[$j] < $maxNumber)
			 || ($arr[$j] == 2 && $maxNumber == 1) 
			)
			{
				$maxNumber = $arr[$j];
				$maxKey = $j;
			}
		}
			$temp = $arr[$i];
			$arr[$i] = $maxNumber;
			$arr[$maxKey] = $temp;
	}
}



?>