<?php
//7. В  массиве  А(N)  найти  номер  элемента,  сумма  которого  со  следующим за ним элементом максимальна, и номер элемента, сумма которого с предыдущим элементом минимальна.  

require "ArrayChapter.php";
class Ex2_7 extends ArrayChapter
{
	function getNextMaxSum()
	{
		$maxSum = $this->arr[0]+$this->arr[1];
		for($i = 0;$i < count($this->arr) - 1; ++$i)
		{
			$sum = $this->arr[$i] + $this->arr[$i+1];
			if($sum >= $maxSum)
			{
				$maxSum = $sum;
				$key = $i;
			}
		}
		return $key;
	}

	function getPrevMinSum()
	{
		$minSum = $this->arr[0]+$this->arr[1];
		for($i = 1;$i < count($this->arr); ++$i)
		{
			$sum = $this->arr[$i] + $this->arr[$i-1];
			if($sum <= $minSum)
			{
				$minSum = $sum;
				$key = $i;
			}
		}
		return $key;
	}
	function execute()
	{
		echo "Номер первого элемента - " . self::getNextMaxSum();
		echo "\nНомер второго элемента - " . self::getPrevMinSum();
	}
}

$array = new Ex2_7;
$array->print();
$array->execute();
?>