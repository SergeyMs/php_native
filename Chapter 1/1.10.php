<?

//10. Найти все меньшие N числа-палиндромы, которые при возведении в квадрат дают палиндром. Число называется палиндромом, если его запись читается одинаково с начала и с конца.

$N = (int)readline("Enter N - ");

for ($i=0;$i<$N;++$i) {
	$value = $i;
	$valueSqr = $value**2;

	if (!checkLastChar($value)) continue;
	if (checkPalindrome($value)) {
		if (checkPalindrome($valueSqr)) {
			echo "$i ->" . $i**2 . "\n";
		}
	}
}


function checkPalindrome($value) {
	$temp = $value;
	$numInverted = 0;
	while ($value >= 1) {
		$num = $value % 10;
		$value = $value / 10;
		$numInverted = $numInverted*10 + $num;
	}
	
	if ($temp == $numInverted) {
		return true;
	} else {
		return false;
	}
}

function checkLastChar($value) { //если последняя цифра не 1 или 2 то палиндрома точно не будет 
	$num = $value % 10;
	if ($value <= 3 || $num <= 2) {
		return true;
	} else {
		return false;
	}
}