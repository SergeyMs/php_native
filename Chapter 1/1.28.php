<?
//28. Преобразовать  числа  заданной  последовательности  натуральных   чисел  n0 ,n1,...,nm так, чтобы цифры каждого числа образовывали максимально возможные числа.     

$N = readline("Enter N - ");
$M = readline("Enter M - ");

for ($i = $N;$i <= $M;++$i) {
	print(sortNumber($i). "\n");
}



function sortNumber($value) {
	$digitNumber = 1;
	$sortedNumber = 0;

	while ($value > 0) {
		$minDigit = $value % 10;
		$digitIndex = $minDigitIndex = 0;
		$temp = $value;

		while ($temp >= 1) { 
			$num = $temp % 10;

			if ($minDigit >= $num) { 
				$minDigit = $num;
				$minDigitIndex = $digitIndex;
				$unsortedNumber = (int)($temp / 10) * (10**$digitIndex); 
			}
			$digitIndex++;
			$temp = $temp / 10;
		}

		$sortedNumber += $digitNumber*$minDigit; 
		$digitNumber *= 10;
		$value = $unsortedNumber + ($value % (10**$minDigitIndex)); 
	}
	return $sortedNumber;
}