<?
//30. Найти среди натуральных чисел n, n+1,...,2n1 числа-близнецы, т. е. два таких простых числа, разность между которыми равна двум.  

$N = readline("Enter N - ");
$M = readline("Enter M - ");


for ($i = $N;$i <= $M-2;++$i) {
	if (checkPrime($i)) {
		if (checkPrime($i + 2)) {
			print("$i ->". ($i + 2). "\n");
		}
	}
}


function checkPrime($value) {
	for($i = 2 ; $i <= sqrt($value) ; $i++) {
		if ($value % $i == 0) {
			return false;
		}

	}
	return true;
}