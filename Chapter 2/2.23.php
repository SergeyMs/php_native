<?php
//23. В массиве А(N) найти минимальное из чисел, встречающихся более одного раза.  
require "ArrayChapter.php";
class Ex2_23 extends ArrayChapter
{
	function getMinNonUniqueElement($arr)
	{
		$arSameElements = array();
		$minElement = $arr[0];	
		for($i = 0; $i < count($arr); ++$i)
		{
			if(!isset($arSameElements[$arr[$i]]))
			{
				$arSameElements[$arr[$i]] = 0;

			}
			$arSameElements[$arr[$i]]++;
			if($arSameElements[$arr[$i]] == 2
				&& isset($minElement)
				&& $minElement >= $arr[$i])
			{
				$minElement = $arr[$i];
			}
		}
		return $minElement;
	}
	function execute()
	{
		print_r(self::getMinNonUniqueElement($this->arr));
	}
}

$array = new Ex2_23;
$array->print();
$array->execute();
?>