<?
//18. Два  натуральных  числа  называют  дружественными,  если  каждое  из них равно сумме всех делителей другого. Найти все пары дружественных чисел, лежащих в диапазоне от N до M. 

$N = readline("Enter N - ");
$M = readline("Enter M - ");


for ($i = $N;$i <= $M; ++$i) {
	for ($j = $i+1; $j <= $M; ++$j) {
		if (dividersSum($i) == $j && dividersSum($j) == $i) {
			echo "$i -> $j\n";
		}
	}
}




function dividersSum($value) {
	$sum = 0;

	for ($i=1;$i<=$value/2;++$i) {
		if ($value % $i == 0) {
			$sum += $i;
		}
	}
	return $sum;
}