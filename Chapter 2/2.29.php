<?php
//29. Из  массива  А(N)  удалить  все  элементы,  стоящие  между  первым  минимальным и последним максимальным элементами.  
$arr = [5, 15, 20, -2, 15, 3, 18, 7, -5, -3, 10, -6, 9, -1, -1, 2, 17, -6, -2, 19];
$maxElement = $minElement = $arr[0];
for($i = 0; $i < count($arr); ++$i)
{
	if($maxElement <= $arr[$i])
	{
		$maxElement = $arr[$i];
		$maxKey = $i;
	}
	if($minElement > $arr[$i])
	{
		$minElement = $arr[$i];
		$minKey = $i;
	}
}
if($maxKey < $minKey)
{
	$temp = $minKey;
	$minKey = $maxKey;
	$maxKey = $temp;
}
for($i = $minKey + 1; $i < $maxKey; ++$i)
{
	unset($arr[$i]);
}
print_r($arr);
?>