<?
//26. Среди натуральных чисел  n0 ,n1,...,nm найти число с максимальной  суммой делителей.  

$N = readline("Enter N - ");
$M = readline("Enter M - ");

$maxSum = $num = 0;

for ($i = $N;$i <= $M;++$i) {
	if ($maxSum <= dividersSum($i)) {
		$maxSum = dividersSum($i);
		$num = $i;
	}
}

echo $num;


function dividersSum($value) {
	$sum = 0;

	for ($i=1;$i<=$value/2;++$i) {
		if ($value % $i == 0) {
			$sum += $i;
		}
	}
	return $sum;
}