<?php
//8. В массиве А(N) поменять местами последний отрицательный элемент с максимальным элементом.  
require "ArrayChapter.php";
class Ex2_8 extends ArrayChapter
{
	function getLastNegativeNum()
	{
		for ($i = count($this->arr) - 1; $i > 0; --$i)
		{
			if ($this->arr[$i] < 0)
			{
				return array(
					"key" => $i,
					"value" => $this->arr[$i]
				); 
			}
		}

		return false;
	}
	function getMax()
	{
		$maxNum = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] >= $maxNum)
			{
				$maxNum = $this->arr[$i];
				$key = $i;
			}
		}
		return array(
					"key" => $key,
					"value" => $maxNum,
				);
	}
	function swapNumbers()
	{
		$num1 = self::getMax();
		$num2 = self::getLastNegativeNum();
		if ($num1 && $num2)
		{
			$this->arr[$num1["key"]] = $num2["value"];
			$this->arr[$num2["key"]] = $num1["value"];
			return true;
		}
		else
		{
			return false;
		}
	
	}


	function execute()
	{
		if(self::swapNumbers())
		{
			echo "\nNew array - \n";
			print_r($this->arr);
		}
		else
		{
			echo "error";
		}
		
	}
}

$array = new Ex2_8;
$array->print();
$array->execute();

?>