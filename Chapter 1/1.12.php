<?
//12. Определить, является ли заданное целое число N простым.

$N = (int)readline("Enter n - ");

if (checkPrime($N)) {
	echo "$N - простое";
} else {
	echo "$N - составное";
}


function checkPrime($value) {
	for($i = 2 ; $i <= sqrt($value) ; $i++) {
		if ($value % $i == 0) {
			echo "$value -> $i\n";
			return false;
		}

	}
	return true;
}