<?php
//19. В массиве А(N) найти последний четный положительный элемент,  кратный заданному числу р, заменить его индексом и поставить в  конец  массива.  
require "ArrayChapter.php";
class Ex2_19 extends ArrayChapter
{
	function getLastPositiveElement($p)
	{
		for($i = count($this->arr) - 1;$i > 0; --$i)
		{
			if($this->arr[$i] % 2 == 0 && $this->arr[$i] > 0 && $this->arr[$i] % $p == 0)
			{
				return $i;
			}
		}
		return false;
	}

	function execute()
	{
		$key = self::getLastPositiveElement(readline("Enter p - "));
		if(isset($key))
		{			
			$this->arr[] = $key;
			$this->arr[$key] = $key;
			print_r($this->arr);
		}
		else
		{
			echo "error";
		}		
	}
}

$array = new Ex2_19;
$array->print();
$array->execute();
?>