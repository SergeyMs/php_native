<?
//15. Определить количество различных делителей целого числа N.  

$N = readline("Enter N - ");

print(dividersCounter($N));



function dividersCounter($value) {
	$counter = 0;
	if ($value < 0) $value = -$value;
	if ($value == 0) return 0;
	for ($i=1;$i<=$value/2;++$i) {
		if ($value % $i == 0) {
			$counter++;
			//echo "$i\n";
		}
	}
	return ++$counter;
}