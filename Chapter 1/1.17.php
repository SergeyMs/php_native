<?
//17. Найти наибольший общий делитель (НОД) двух натуральных чисел N и M.   

$N = readline("Enter N - ");
$M = readline("Enter M - ");

print(nod($N,$M));



function nod($n, $m) {

	if ($m > $n) {
		$temp = $n;
		$n = $m;
		$m = $temp;
	}

	while ($n >= $m) {
		$n -= $m;
		if ($n == 0) {
			return $m;
		}
	}
	return nod($n,$m);
}