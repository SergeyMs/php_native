<?
//20. Найти целое число в диапазоне от N до M с наибольшей суммой  делителей.  

$N = readline("Enter N - ");
$M = readline("Enter M - ");

$maxSum = $maxDigit = 0;
for ($i = $N;$i <= $M; ++$i) {
	if (dividersSum($i) >= $maxSum) {
		$maxSum = dividersSum($i);
		$maxDigit = $i;
	}
}
echo "$maxDigit\n";




function dividersSum($value) {
	$sum = 0;

	for ($i=1;$i<=$value/2;++$i) {
		if ($value % $i == 0) {
			$sum += $i;
		}
	}
	return $sum;
}