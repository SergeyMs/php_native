<?php
//17. В массиве А(N) подсчитать количество элементов, встречающихся  более одного раза.  
require "ArrayChapter.php";
class Ex2_17 extends ArrayChapter
{
	function sameElementLength($arr)
	{
		$count = 0;
		$arSameElements = array();
		for($i = 0; $i < count($arr); ++$i)
		{
			if(!isset($arSameElements[$arr[$i]]))
			{
				$arSameElements[$arr[$i]] = 0;	
			}
			$arSameElements[$arr[$i]]++;
			if($arSameElements[$arr[$i]] == 2)
			{
				$count++;
			}
		}
		return $count;
	}
	function execute()
	{
		echo "\ncount - " . self::sameElementLength($this->arr);
	}
}

$array = new Ex2_17;
$array->print();
$array->execute();
?>