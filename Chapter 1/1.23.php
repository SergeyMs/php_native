<?
//23. Дано  целое  число  N.  Преобразовать  число  так, чтобы его цифры  следовали в порядке возрастания.  

$N = readline("Enter N - ");


print(sortNumber($N));


function sortNumber($value) {
	$digitNumber = 1;
	$sortedNumber = 0;

	while ($value > 0) {
		$minDigit = $value % 10;
		$digitIndex = $minDigitIndex = 0;
		$temp = $value;

		while ($temp >= 1) { //Ищем максимальный разряд в несортированном числе
			$num = $temp % 10;

			if ($minDigit <= $num) { //Если находим, то запоминаем номер разряда и цифру
				$minDigit = $num;
				$minDigitIndex = $digitIndex;
				$unsortedNumber = (int)($temp / 10) * (10**$digitIndex); //первая часть несортированного числа до максимального разряда
			}
			$digitIndex++;
			$temp = $temp / 10;
		}

		$sortedNumber += $digitNumber*$minDigit; //Формируем новое число по разрядам справа налево
		$digitNumber *= 10;
		$value = $unsortedNumber + ($value % (10**$minDigitIndex)); //первая + вторая часть несортированного числа без максимального разряда
	}
	return $sortedNumber;
}





