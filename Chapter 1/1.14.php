<?
//14. Парными  простыми числами называются два простых числа, разность  которых равна двум. Например,  3 и 5; 11 и 13. Найти 10  парных  простых чисел.  

$counter = 0;
$primeDigit = 3;

while ($counter < 10) {
	if (checkPrime($primeDigit)) {
		if (checkPrime($primeDigit + 2)) {
			$counter++;
			print("$primeDigit ->". ($primeDigit + 2). "\n");
		}
	}
	$primeDigit += 2;
}


function checkPrime($value) {
	for($i = 2 ; $i <= sqrt($value) ; $i++) {
		if ($value % $i == 0) {
			return false;
		}

	}
	return true;
}