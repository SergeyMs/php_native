<?
//16. Натуральное число называют совершенным, если оно равно сумме всех своих делителей, не считая его самого. Например, 6=1+2+3.  Найти  все совершенные числа в диапазоне от N до M.  

$N = readline("Enter N - ");
$M = readline("Enter M - ");


for ($i = $N;$i <= $M;++$i) {
	if ($i == dividersSum($i)) {
		echo "$i\n";
	}
}



function dividersSum($value) {
	$sum = 0;

	for ($i=1;$i<=$value/2;++$i) {
		if ($value % $i == 0) {
			$sum += $i;
		}
	}
	return $sum;
}