<?php
//13. В  массиве  А(N)  найти  два  наименьших  элемента  и  два  наибольших элемента.  
require "ArrayChapter.php";
class Ex2_13 extends ArrayChapter
{
	function get2Max()
	{
		for($i = 0; $i < count($this->arr); ++$i)
		{			
			if(!isset($maxNum))
			{
				$maxNum = $this->arr[$i];
			}
			if($this->arr[$i] >= $maxNum)
			{
				$maxNum2 = $maxNum;
				$maxNum = $this->arr[$i];
			}
		}
		return "$maxNum -> $maxNum2";
	}
	function get2Min()
	{
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if(!isset($minNum))
			{
				$minNum = $this->arr[$i];
			}
			if($this->arr[$i] <= $minNum)
			{
				$minNum2 = $minNum;
				$minNum = $this->arr[$i];
			}
		}
		return "$minNum -> $minNum2";
	}
	function execute()
	{
		echo "\nдва наибольших - " . self::get2Max();
		echo "\nдва наименьших - " . self::get2Min();
	}
}
$array = new Ex2_13;
$array->print();
$array->execute();
?>