<?
//27. Среди натуральных чисел  n0 ,n1,...,nm найти число с максимальной  суммой простых делителей.  

$N = readline("Enter N - ");
$M = readline("Enter M - ");



$maxSum = $num = 0;

for ($i = $N;$i <= $M;++$i) {
	if ($maxSum <= dividersSum($i)) {
		$maxSum = dividersSum($i);
		$num = $i;
	}
}

echo $num;



function dividersSum($value) {
	$sum = 0;

	for ($i=1;$i<=$value/2;++$i) {
		if ($value % $i == 0 && checkPrime($i)) {
			$sum += $i;
		}
	}
	return $sum;
}


function checkPrime($value) {
	for($i = 2 ; $i <= sqrt($value) ; $i++) {
		if ($value % $i == 0) {
			return false;
		}

	}
	return true;
}