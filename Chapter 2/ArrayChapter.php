<?php


abstract class ArrayChapter
{
	public $arr = array();

	function __construct()
	{
		self::fillArray();
	}
	
	function fillArray($length = 20, $leftNumber = -10, $rightNumber = 20)
	{
		unset($this->arr);
		for ($i = 0; $i < $length; ++$i)
		{
			$this->arr[$i] = rand($leftNumber, $rightNumber);
		}
	}

	function print()
	{
		print_r($this->arr);
	}
}

?>