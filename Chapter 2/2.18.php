<?php
//18. В массиве А(N) подсчитать количество различных элементов.  
require "ArrayChapter.php";
class Ex2_18 extends ArrayChapter
{
	function diffElementCount($arr)
	{
		$count = 0;
		$arSameElements = array();
		for($i = 0; $i < count($arr); ++$i)
		{
			if(!isset($arSameElements[$arr[$i]]))
			{
				$arSameElements[$arr[$i]] = 0;	
			}
			$arSameElements[$arr[$i]]++;
			if($arSameElements[$arr[$i]] == 1)
			{
				$count++;
			}
		}
		return $count;
	}
	function execute()
	{
		echo "\ncount - " . self::diffElementCount($this->arr);
	}
}

$array = new Ex2_18;
$array->print();
$array->execute();

?>