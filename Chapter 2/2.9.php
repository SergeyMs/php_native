<?php
//9. В  массиве  А(N)  найти  максимальный  среди  четных  элементов  и  минимальный среди нечетных.  
require "ArrayChapter.php";
class Ex2_9 extends ArrayChapter
{
	function getEvenMax()
	{
		$maxNum = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] >= $maxNum && $this->arr[$i] % 2 == 0)
			{
				$maxNum = $this->arr[$i];
			}
		}
		return $maxNum;
	}
	function getOddMin()
	{
		$minNum = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] <= $minNum && $this->arr[$i] % 2 != 0)
			{
				$minNum = $this->arr[$i];
			}
		}
		return $minNum;
	}
	function execute()
	{
		echo "\nмаксимальный среди четных - " . self::getEvenMax();
		echo "\nминимальный среди нечетных - " . self::getOddMin();
	}
}

$array = new Ex2_9;
$array->print();
$array->execute();
?>