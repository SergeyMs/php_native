<?php
//Определить,    есть  ли  среди  элементов  массива  А(N)  последовательность из k элементов, равных числу х.  
require "ArrayChapter.php";
class Ex2_15 extends ArrayChapter
{
    function sequence_exists($arr, $number)
    {
        for($i = 0; $i < count($arr); ++$i)
        {
            $sum = 0;
            for($j = $i;$j < count($arr); ++$j)
            {
                $sum += $arr[$j];
                if ($sum == $number)
                {
                    echo "\ni -> $i\nj -> $j\n";
                    return true;
                }
            }
        }
    }
    function execute()
    {
        if(self::sequence_exists($this->arr, readline("Enter x - ")))
        {
            echo "последовательность есть";
        }
        else
        {
            echo "последовательности нет";
        }
    }
}

$array = new Ex2_15;
$array->print();
$array->execute();
?>
