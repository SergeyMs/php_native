<?php
//21. В массиве А(N) выбрать без повторений все элементы, встречающиеся более одного раза.  
require "ArrayChapter.php";
class Ex2_21 extends ArrayChapter
{
	function getNonUniqueElements($arr)
	{
		$arSameElements = array();
		for($i = 0; $i < count($arr); ++$i)
		{
			if(!isset($arSameElements[$arr[$i]]))
			{
				$arSameElements[$arr[$i]] = 0;	
			}
			$arSameElements[$arr[$i]]++;
			if($arSameElements[$arr[$i]] == 2)
			{
				$arResult[] = $arr[$i];
			}
		}
		return $arResult;
	}
	function execute()
	{
		print_r(self::getNonUniqueElements($this->arr));
	}
}

$array = new Ex2_21;
$array->print();
$array->execute();
?>