<?
//25. Среди      заданной      последовательности      натуральных      чисел    n0 ,n1,...,nm найти  сумму  и  количество  тех  чисел,  которые  равны  сумме  факториалов своих цифр.   

$N = readline("Enter N - ");
$M = readline("Enter M - ");

$sum = $count = 0;

for ($i = $N;$i <= $M;++$i) {
	if ($i == factorialSum($i)) {
		$sum += $i;
		$count++;
		//print("$i ->". factorialSum($i) . "\n");
	}
}
echo "Sum - $sum || count - $count";



function factorialSum($value) {
	$sum = 0;
	$factorial = 1;

	while ($value >= 1) {
		$digit = $value % 10;
		$value /= 10;
		if ($digit == 0) continue;

		for ($i = 1;$i <= $digit; ++$i) {
			$factorial *= $i;
		}

		$sum += $factorial;
		$factorial = 1;
		

	}

	return $sum;
}