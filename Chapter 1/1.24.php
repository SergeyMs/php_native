<?
//24. Напечатать  натуральное  число  N :  а)  в  двоичной  системе  счисления;  б) в шестнадцатеричной системе счисления.  

$N = readline("Enter N - ");

printf("^2 -> %b\n", convertToNewBase($N, 2));
printf("^16 -> %x\n", convertToNewBase($N, 16));


function convertToNewBase($value, $base) {
	$number = 0;
	$digitIndex = 0;

	while ($value >= 1) {
		$digit = $value % $base;

		if ($base == 2) {
			$number += 0b10**$digitIndex*$digit;
		}

		if ($base == 16) {
			$number += 0x10**$digitIndex*$digit;
		}

		$digitIndex++;
		$value = $value / $base;
	}

	return $number;
}



