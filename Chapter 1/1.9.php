<?
//9. Дано  натуральное  число  N.  Определить,  является  ли  оно  автоморфным. Автоморфное число  равно последним разрядам квадрата этого числа. Например,  5 и 25,  6 и 36,  25 и 625.

$N = (int)readline("Enter N - ");
$sqrN = $N**2;

if (checkAutomorph($N, $sqrN)) {
	echo "$N является автоморфным";
} else {
	echo "$N не является автоморфным";
}




function checkAutomorph($N, $sqrN) {
	while ($N>=1) {
		$numN = $N % 10;
		$numSqrN = $sqrN % 10;

		if ($numN != $numSqrN) return false;

		$N = $N / 10;
		$sqrN = $sqrN / 10;
		
	}
	return true;
}
