<?
//13. Определить, является ли число  2n  m  симметричным, т. е. запись  числа содержит четное количество цифр и совпадают его левая и правая  половинки.  

$N = readline("Enter N - ");

if (checkSymmetry($N)) {
	echo "$N - симметричное";
} else {
	echo "$N - несимметричное";
}



function checkSymmetry($value) {
	$N = $value;
	$numDigit = $numInverted = 0;
	while ($value >= 1) {
		$num = $value % 10;
		$value = $value / 10;
		$numDigit++;
		$numInverted = $numInverted*10 + $num;

	}
	if ($numInverted == $N && $numDigit % 2 == 0) {
		return true;
	} else {
		return false;
	}

}