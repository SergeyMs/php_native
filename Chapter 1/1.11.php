<?

//11. Напечатать   те   элементы   последовательности   натуральных   чисел n0 ,n1,...,nm , которые делятся на сумму своих цифр.

$N = readline("Enter n - ");
$M = readline("Enter m - ");

for ($i=$N;$i<=$M;$i++) {

	if (($i % digitSum($i)) == 0) {
		echo "$i\n";
	}

}


function digitSum($value) {
	$sum = 0;
	while ($value >= 1) {
		$num = $value % 10;
		$value = $value / 10;
		$sum += $num;
	}
	return $sum;
}