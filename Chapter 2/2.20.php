<?php
//20. В  массиве  А(N)  выбрать  все  элементы,  встречающиеся  только  один раз.  
require "ArrayChapter.php";
class Ex2_20 extends ArrayChapter
{
	function getUniqueElements($arr)
	{
		for($i = 0; $i < count($arr); ++$i)
		{
			if(self::isUniqueElement($arr, $arr[$i]))
			{
				$arUniqueElements[] = $arr[$i];
			}
		}
		return $arUniqueElements;
	}
	function isUniqueElement($arr, $value)
	{
		$count = 0;
		for ($i = 0;$i < count($arr); ++$i)
		{
			if ($arr[$i] == $value)
			{
				$count++;
			}
			if ($count > 1) return false;
		}
		return true;
	}
	function execute()
	{
		print_r(self::getUniqueElements($this->arr));
	}
}

$array = new Ex2_20;
$array->print();
$array->execute();
?>