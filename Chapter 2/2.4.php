<?php
//4. В  массиве  А(N)  найти  количество  пар  одинаковых  соседних элементов.  
require "ArrayChapter.php";
class Ex2_4 extends ArrayChapter
{
	function getNeighbours()
	{
		$count = 0;
		for ($i = 0; $i < count($this->arr)-1; ++$i)
		{
			if ($this->arr[$i] == $this->arr[$i+1])
			{
				$count++;
			}
		}
		return $count;
	}

	function execute()
	{
		echo "Число пар - " . self::getNeighbours();
	}
}

$array = new Ex2_4;
$array->print();
$array->execute();

?>