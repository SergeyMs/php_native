<?php
//3. В массиве А(N) первый положительный элемент и последний отрицательный элемент переставить местами.  

require "ArrayChapter.php";
class Ex2_3 extends ArrayChapter
{

	function getFirstPositiveNum()
	{
		for ($i = 0; $i < count($this->arr); ++$i)
		{
			if ($this->arr[$i] > 0)
			{
				return array(
					"key" => $i,
					"value" => $this->arr[$i]
				);
			}
		}
		return false;
	}

	function getLastNegativeNum()
	{
		for ($i = count($this->arr) - 1; $i >= 0; --$i)
		{
			if ($this->arr[$i] < 0)
			{
				return array(
					"key" => $i,
					"value" => $this->arr[$i]
				); 
			}
		}
		return false;
	}

	function swapNumbers()
	{
		$num1 = self::getFirstPositiveNum();
		$num2 = self::getLastNegativeNum();
		if ($num1 && $num2)
		{
			$this->arr[$num1["key"]] = $num2["value"];
			$this->arr[$num2["key"]] = $num1["value"];
			return true;
		}
		else
		{
			return false;
		}
	
	}

	function execute()
	{
		if(self::swapNumbers())
		{
			echo "New array - \n";
			print_r($this->arr);
		}
		else
		{
			echo "error";
		}
		
	}


}

$array = new Ex2_3;
$array->print();
$array->execute();

?>