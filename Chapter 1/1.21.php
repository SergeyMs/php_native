<?
//21. Даны  натуральные  числа  N  и  M.  Определить,  являются  ли  они  взаимно простыми числами. Взаимно простые числа не имеют общих делителей, кроме единицы.  

$N = readline("Enter N - ");
$M = readline("Enter M - ");

if (nod($N,$M) == 1) {
	echo "true";
} else {
	echo "false";
}



function nod($n, $m) {

	if ($m > $n) {
		$temp = $n;
		$n = $m;
		$m = $temp;
	}

	while ($n >= $m) {
		$n -= $m;
		if ($n == 0) {
			return $m;
		}
	}
	return nod($n,$m);
}