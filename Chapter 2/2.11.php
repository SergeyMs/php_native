<?php
//11. В массиве А(N) найти первый отрицательный элемент, предшествующий максимальному элементу, и последний положительный элемент,  стоящий за минимальным элементом.  
require "ArrayChapter.php";
class Ex2_11 extends ArrayChapter
{
	function getPrevMaxNegative()
	{
		$maxNum = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] >= $maxNum)
			{
				$maxNum = $this->arr[$i];
				$key = $i;
			}
		}
		for($i = $key - 1;$i >= 0; --$i)
		{
			if ($this->arr[$i] < 0)
			{
				return $this->arr[$i];
			}

		}
		return false;
	}
	function getLastMinPositive()
	{
		$minNum = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] <= $minNum)
			{
				$minNum = $this->arr[$i];
				$key = $i;
			}
		}
		for($i = count($this->arr) - 1;$i >= $key + 1; --$i)
		{
			if ($this->arr[$i] > 0)
			{
				return $this->arr[$i];
			}

		}
		return false;
	}
	function execute()
	{
		echo "\nпервый отрицательный элемент, предшествующий максимальному элементу - " . self::getPrevMaxNegative();
		echo "\nпоследний положительный элемент,  стоящий за минимальным элементом - " . self::getLastMinPositive();
	}
}

$array = new Ex2_11;
$array->print();
$array->execute();
?>