<?php
//14. Разделить заданный массив А(N) на два массива:  
 //       а) массив положительных и отрицательных элементов;  
 //       б) массив четных и нечетных элементов.  
require "ArrayChapter.php";
class Ex2_14 extends ArrayChapter
{
	function explodeArray()
	{
		for($i = 0;$i < count($this->arr); ++$i)
		{
			if($this->arr[$i] > 0)
			{
				$arrAPos[] = $this->arr[$i];
			}
			else if ($this->arr[$i] < 0)
			{
				$arrANeg[] = $this->arr[$i];
			}

			if($this->arr[$i] % 2 == 0)
			{
				$arrBEven[] = $this->arr[$i];
			}
			else
			{
				$arrBOdd[] = $this->arr[$i];
			}
		}
		return array(
			"A positive" => $arrAPos,
			"A negative" => $arrANeg,
			"B Even" => $arrBEven,
			"B Odd" => $arrBOdd
		);
	}
	function execute()
	{
		print_r(self::explodeArray());
	}
}
$array = new Ex2_14;
$array->print();
$array->execute();
?>