<?php
//1. В    массиве  А(N)  найти:    а)  число  элементов,  предшествующих  первому отрицательному элементу;  б) сумму нечетных элементов массива, следующих за последним отрицательным элементом.  
require_once "ArrayChapter.php";

class Ex2_1 extends ArrayChapter
{

	function getCountBeforeFirstNegative()
	{
		$counter = 0;
		for ($i = 0; $i < count($this->arr); ++$i)
		{
			if ($this->arr[$i] >= 0)
			{
				$counter++;
			} 
			else
			{
				break;
			}
		}
		return $counter;
	}

	function getSumAfterLastNegative()
	{
		$sum = 0;
		for ($i = count($this->arr)-1; $i >= 0; --$i)
		{
			if ($this->arr[$i] >= 0 && $this->arr[$i] % 2 != 0)
			{
				$sum += $this->arr[$i];
			}
			else
			{
				if ($this->arr[$i] < 0) break;
			}
		}
		return $sum;
	}
}


$array = new Ex2_1;
$array->print();
echo "Число элеметов до первого отрицательного - " . $array->getCountBeforeFirstNegative();
echo "\nCумма нечетных после последнего отрицательного - " . $array->GetSumAfterLastNegative();
?>
