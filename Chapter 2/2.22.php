<?php
//22. В массиве А(N) найти максимальный из элементов, встречающихся только один раз.  
require "ArrayChapter.php";
class Ex2_22 extends ArrayChapter
{
	function getMaxUniqueElements($arr)
	{
		
		for($i = 0; $i < count($arr); ++$i)
		{
			if(!isset($maxUniqueElement)
				&& self::isUniqueElement($arr, $arr[$i]))
			{
				$maxUniqueElement = $arr[$i];
			}
			if(self::isUniqueElement($arr, $arr[$i]) 
				&& isset($maxUniqueElement) 
				&& $maxUniqueElement <= $arr[$i])
			{
				$maxUniqueElement = $arr[$i];
			}
		}
		return $maxUniqueElement;
	}
	function isUniqueElement($arr, $value)
	{
		$count = 0;
		for ($i = 0;$i < count($arr); ++$i)
		{
			if ($arr[$i] == $value)
			{
				$count++;
			}
			if ($count > 1) return false;
		}
		return true;
	}
	function execute()
	{
		print_r(self::getMaxUniqueElements($this->arr));
	}
}

$array = new Ex2_22;
$array->print();
$array->execute();

?>