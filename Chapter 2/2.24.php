<?php
//24. Из  всех  участков массива А(N), сплошь состоящих из нулей, выбрать самый длинный и отметить индексы его начала и конца.  
require "ArrayChapter.php";
class Ex2_24 extends ArrayChapter
{
	function getMaxSequence($arr, $number)
	{
		$sequenceCount = $sequenceFrom = $maxSequenceCount = 0;
		for($i = 0; $i < count($arr); ++$i)
		{			
			if($arr[$i] == $number)
			{
				if($sequenceCount == 0)
				{
					$sequenceFrom = $i;
				}
				$sequenceCount++;
			}
			else
			{
				
				$sequenceCount = 0;
			}

			if($sequenceCount >= $maxSequenceCount)
				{
					$maxSequenceCount = $sequenceCount;
					$maxSequenceFrom = $sequenceFrom;
				}
		}
		if ($maxSequenceCount == 0)
		{
			return false;
		}
		else
		{
			return array(
			"from" => $maxSequenceFrom,
			"to" => $maxSequenceFrom + $maxSequenceCount - 1,
			);
		}	
	}

	function execute()
	{
		print_r(self::getMaxSequence($this->arr, 0));
	}
}

$array = new Ex2_24;
$array->fillArray(20, 0, 10);
$array->print();
$array->execute();
?>