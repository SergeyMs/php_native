<?php
//5. В массиве А(N) найти максимальный и минимальный элементы и  переставить их местами.  
require "ArrayChapter.php";
class Ex2_5 extends ArrayChapter
{
	function getMinMax()
	{
		$maxNum["value"] = $minNum["value"] = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] >= $maxNum["value"])
			{
				$maxNum["value"] = $this->arr[$i];
				$maxNum["key"] = $i;
			}
			if($this->arr[$i] <= $minNum["value"])
			{
				$minNum["value"] = $this->arr[$i];
				$minNum["key"] = $i;
			}
		}
		return array($minNum, $maxNum);
	}

	function swapNumbers()
	{
		$nums = self::getMinMax();
		if ($nums)
		{
			$this->arr[$nums[0]["key"]] = $nums[1]["value"];
			$this->arr[$nums[1]["key"]] = $nums[0]["value"];
			return true;
		}
		else
		{
			return false;
		}
	
	}

	function execute()
	{
		if(self::swapNumbers())
		{
			echo "New array - \n";
			print_r($this->arr);
		}
		else
		{
			echo "error";
		}
		
	}
}


$array = new Ex2_5;
$array->print();
$array->execute();
?>