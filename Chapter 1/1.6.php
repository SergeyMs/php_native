<? 
//6. Получить  все  четырехзначные  числа,  в  записи  которых  встречаются только цифры 0,2,3,7.

for ($i=1000;$i<=9999;$i++) {
	$value = $i;
	$cond_0 = $cond_2 = $cond_3 = $cond_7 = false;

	while ($value >= 1) {
		$num = $value % 10;
		$value = $value / 10;

		switch ($num) {
			case 0: $cond_0 = true;
					break;
			case 2: $cond_2 = true;
					break;
			case 3: $cond_3 = true;
					break;
			case 7: $cond_7 = true;
					break;
		}

	}
	if ($cond_0 && $cond_2 && $cond_3 && $cond_7) echo "$i\n";

}

