<?php
//16. В  массиве  А(N)  определить  максимальную  длину последовательности равных элементов.  
require "ArrayChapter.php";
class Ex2_16 extends ArrayChapter
{
	function sequenceLength($arr)
	{
		$prevNum = $arr[0];
		$length = $maxLength = 1;		
		for($i = 1; $i < count($arr); ++$i)
		{
			if ($arr[$i] == $prevNum)
			{
				$length++;

			}
			else
			{
				$prevNum = $arr[$i];
				if($maxLength < $length)
				{
					$maxLength = $length;
				}
				$length = 1;
			}
			if($maxLength < $length)
				{
					$maxLength = $length;
				}
		}
		return $maxLength;
	}
	function execute()
	{
		echo "Длина последовательности - " . self::sequenceLength($this->arr);
	}
}

$array = new Ex2_16;
$array->print();
$array->execute();
?>