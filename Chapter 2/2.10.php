<?php
//10. В  массиве  А(N)  все  четные  элементы  заменить  максимальным  элементом,  а нечетные  минимальным.
require "ArrayChapter.php";
class Ex2_10 extends ArrayChapter
{
	function getMax()
	{
		$maxNum = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] >= $maxNum)
			{
				$maxNum = $this->arr[$i];
			}
		}
		return $maxNum;
	}
	function getMin()
	{
		$minNum = $this->arr[0];
		for($i = 0; $i < count($this->arr); ++$i)
		{
			if($this->arr[$i] <= $minNum)
			{
				$minNum = $this->arr[$i];
			}
		}
		return $minNum;
	}
	function replaceNums($order, $value)
	{
		$order == "even" ? $i = 0 : false;
		$order == "odd"  ? $i = 1 : false;
		while($i <= count($this->arr) - 1)
		{
			$this->arr[$i] = $value;
			$i += 2;
		}
	}
	function execute()
	{
		self::replaceNums("even", self::getMax());
		self::replaceNums("odd", self::getMin());
		echo "\nNew array - \n";
		print_r($this->arr);
	}
}

$array = new Ex2_10;
$array->print();
$array->execute();
?>