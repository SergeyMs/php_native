<? 
// 7. Выяснить, есть ли в записи натурального числа N две одинаковые цифры.

$value = (int)readline("Enter number: ");
echo "В числе $value ";

while ($value >= 1) {
	$num = $value % 10;
	$value = $value / 10;
	$cond = checkMatches($num, $value);
	if ($cond) break;
}

if ($cond) {
	die("есть две одинаковые цифры");	
} else {
	die("нет двух одинаковых цифр");
}

function checkMatches($numPrev, $value) {
    while ($value >=1) {
        $num = $value % 10;
        if ($numPrev == $num) {
            return true;
        }
        $value = $value / 10;
    }
    return false;
}







